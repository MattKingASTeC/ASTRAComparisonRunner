"System test"
#pylint: disable=locally-disabled, invalid-name, line-too-long
import unittest
import sys
from PyQt4 import QtGui
import astra_comparison_controller as GUI_to_be_tested
import astra_comparison_data as data
import run_parameters_parser as yaml_parser
class astra_comparison_tester(unittest.TestCase):
    "Tester"
    testValues = ["", "f", 1, 0.1, -1]
    application = QtGui.QApplication(sys.argv)
    GUI_to_be_tested = GUI_to_be_tested.AstraComparisonController(True)
    local_parameters = data.AstraComparisonData().parameters
    list_of_keys = local_parameters.keys()
    def test(self):
        "Test for input"
        for i in range(0, 5):
            print "running test "+ str(self.testValues[i])
            for k in self.list_of_keys:
                self.local_parameters[k] = self.testValues[i]
                if k == "rest_of_line_space_charge" or k == "injector_space_charge":
                    self.local_parameters[k] = "F"
            yaml_parser.write_parameter_output_file('test'+str(i)+'.yml', self.local_parameters)
            self.GUI_to_be_tested.testNum = i
            self.GUI_to_be_tested.import_parameter_values_from_yaml_file()
            self.GUI_to_be_tested.run_astra()
            self.GUI_to_be_tested.export_parameter_values_to_yaml_file()
    def testIfOutIsNotEqual(self):
        "Test for desired output"
        for i in range(0, 5):
            dict1 = yaml_parser.parse_parameter_input_file("test"+str(i)+".yml")
            dict2 = yaml_parser.parse_parameter_input_file("testOut"+str(i)+".yml")
            self.assertNotEqual(dict1, dict2)
if __name__ == '__main__':
    unittest.main()
    
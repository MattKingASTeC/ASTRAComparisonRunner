from collections import OrderedDict


class AstraComparisonData:

    def __init__(self):
        # need to be set to default values for Fortran Script. if not, empty string test will fail on import.
        self.parameters = OrderedDict([('astra_run_number', '001'), ('macro_particle', '1000'), ('laser_pulse_length', '1'),
                                       ('spot_size', '1'), ('charge', '1'), ('gun_gradient', '1'), ('gun_phase', '1'),
                                       ('linac1_gradient', '1'), ('linac1_phase', '1'),
                                       ('bucking_coil_and_sol_strength', '1'), ('linac1_sol1_strength', '1'),
                                       ('linac1_sol2_strength', '1'), ('end_of_line', '1'),
                                       ('injector_space_charge', 'T'), ('s02_quad1_strength', '1'),
                                       ('s02_quad2_strength', '1'), ('s02_quad3_strength', '1'),
                                       ('s02_quad4_strength', '1'), ('c2v_quad1_strength', '1'),
                                       ('c2v_quad2_strength', '1'), ('c2v_quad3_strength', '1'),
                                       ('vela_quad1_strength', '1'), ('vela_quad2_strength', '1'),
                                       ('vela_quad3_strength', '1'), ('vela_quad4_strength', '1'),
                                       ('vela_quad5_strength', '1'), ('vela_quad6_strength', '1'),
                                       ('ba1_quad1_strength', '1'), ('ba1_quad2_strength', '1'),
                                       ('ba1_quad3_strength', '1'), ('ba1_quad4_strength', '1'),
                                       ('ba1_quad5_strength', '1'), ('ba1_quad6_strength', '1'),
                                       ('ba1_quad7_strength', '1'), ('rest_of_line_space_charge', 'F'),
                                       ('directory', 'EMPTY_DIRECTORY')])
        self.commands = []

    def set_parameters(self, key, value):
        if key == "astra_run_number" or key == "directory" or key == "rest_of_line_space_charge" or key == "injector_space_charge":
            self.parameters[key] = value
        elif key == "macro_particle":
            self.parameters[key] = int(value)
        else:
            self.parameters[key] = float(value)

    def get_parameters(self):
        return self.parameters

    def set_commands(self, commands):
        self.commands = commands
